package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
// import java.util.ListIterator;
import java.util.LinkedList;
import java.util.stream.Collectors;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        this.commands.stream()
            .forEach(command -> command.execute());

        // ListIterator<Command> iterator = this.commands.listIterator();
        // while (iterator.hasNext()) {
        //     iterator.next().execute();
        // }
    }

    @Override
    public void undo() {
        this.commands.stream()
            .collect(Collectors.toCollection(LinkedList::new))
            .descendingIterator()
            .forEachRemaining(command -> command.undo());

        // ListIterator<Command> iterator = this.commands.listIterator(this.commands.size());
        // while (iterator.hasPrevious()) {
        //     iterator.previous().undo();
        // }
    }
}
