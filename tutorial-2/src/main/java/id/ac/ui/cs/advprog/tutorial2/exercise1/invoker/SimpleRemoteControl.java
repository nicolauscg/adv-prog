package id.ac.ui.cs.advprog.tutorial2.exercise1.invoker;

import id.ac.ui.cs.advprog.tutorial2.exercise1.command.Command;

public class SimpleRemoteControl {

    private Command slot;

    public void setCommand(Command command) {
        this.slot = command;
    }

    public void buttonWasPressed() {
        this.slot.execute();
    }

    // remote control with naive solution

    // private Object slot;

    // public void setSlot(Object slot) {
    //     this.slot = slot;
    // }

    // public void buttonWasPressed() {
    //     if (slot instanceof Light) {
    //         ((Light)slot).on();
    //     } else if (slot instanceof CeilingFan) {
    //         ((CeilingFan)slot).medium();
    //     }
    // }
}
