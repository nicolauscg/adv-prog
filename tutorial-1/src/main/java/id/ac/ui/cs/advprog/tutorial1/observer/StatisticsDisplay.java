package id.ac.ui.cs.advprog.tutorial1.observer;

import java.util.Observable;
import java.util.Observer;

public class StatisticsDisplay implements Observer, DisplayElement {

    private Observable observable;
    private float maxTemp = 0.0f;
    private float minTemp = 200;
    private float tempSum = 0.0f;
    private int numReadings;

    public StatisticsDisplay(Observable observable) {
        this.observable = observable;
        this.observable.addObserver(this);
        this.numReadings = 0;
    }

    @Override
    public void display() {
        System.out.println("Avg/Max/Min temperature = " + (tempSum / numReadings)
                + "/" + maxTemp + "/" + minTemp);
    }

    @Override
    public void update(Observable o, Object arg) {
        if (o instanceof WeatherData) {
            WeatherData weatherData = (WeatherData)o;
            this.tempSum += weatherData.getTemperature();
            if (weatherData.getTemperature() > this.maxTemp) {
                this.maxTemp = weatherData.getTemperature();
            }
            if (weatherData.getTemperature() < this.minTemp) {
                this.minTemp = weatherData.getTemperature();
            }
            this.numReadings++;
            display();
        }
    }
}
