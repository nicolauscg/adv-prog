package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Cucumber extends Filling {
    public Cucumber(Food food) {
        this.food = food;
        this.description = "cucumber";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.seperator + this.description;
    }

    @Override
    public double cost() {
        return 0.4 + this.food.cost();
    }
}
