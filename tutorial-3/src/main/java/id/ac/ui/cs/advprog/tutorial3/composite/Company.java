package id.ac.ui.cs.advprog.tutorial3.composite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Company {
    protected List<Employees> employeesList;

    public Company() {
        employeesList = new ArrayList<Employees>();
    }

    public Company(List<Employees> employeesList) {
        Collections.copy(this.employeesList, employeesList);
    }

    public void addEmployee(Employees employees) {
        this.employeesList.add(employees);
    }

    public double getNetSalaries() {
        Iterator<Employees> iterator = employeesList.iterator();
        double netSalaries = 0;
        while (iterator.hasNext()) {
            netSalaries += iterator.next().getSalary();
        }

        return netSalaries;
    }

    public List<Employees> getAllEmployees() {
        return this.employeesList;
    }
}
