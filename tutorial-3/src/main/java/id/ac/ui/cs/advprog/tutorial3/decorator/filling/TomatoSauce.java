package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class TomatoSauce extends Filling {
    public TomatoSauce(Food food) {
        this.food = food;
        this.description = "tomato sauce";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.seperator + this.description;
    }

    @Override
    public double cost() {
        return 0.2 + this.food.cost();
    }
}
