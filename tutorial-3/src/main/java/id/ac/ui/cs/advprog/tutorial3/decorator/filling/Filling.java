package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public abstract class Filling extends Food {
    Food food;
    String seperator = ", adding ";
    
    public abstract String getDescription();
}
