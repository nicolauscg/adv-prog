package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    private static final double MINIMUM_SALARY = 100000.00;

    public Cto(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        this.salary = salary;
        this.role = "CTO";
        if (!hasValidSalary()) {
            throw new IllegalArgumentException(
                String.format("%s %s must have minimum salary %.2f\n", 
                    this.role, this.name, MINIMUM_SALARY)
            );
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    protected boolean hasValidSalary() {
        return this.salary >= MINIMUM_SALARY;
    }
}
