package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        Food food = BreadProducer.THICK_BUN.createBreadToBeFilled();
        System.out.println(food.getDescription());
        System.out.println(food.cost());

        food = FillingDecorator.CHEESE.addFillingToBread(food);
        food = FillingDecorator.BEEF_MEAT.addFillingToBread(food);
        System.out.println(food.getDescription());
        System.out.println(food.cost());

        // naive solution with boolean value for each filling
        // Food food = BreadProducer.THICK_BUN.createBreadToBeFilled();
        // food.hasBeefMeat = true;
        // food.hasCheese = true;
        // System.out.println(food.getDescription());
        // System.out.println(food.cost());

    }
}