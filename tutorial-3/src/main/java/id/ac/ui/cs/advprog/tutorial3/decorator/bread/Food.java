package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

public abstract class Food {
    String description = "unknown food";

    public String getDescription() {
        return this.description;
    }

    public abstract double cost();
}