package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    private static final double MINIMUM_SALARY = 50000.00;

    public NetworkExpert(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        this.salary = salary;
        this.role = "Network Expert";
        if (!hasValidSalary()) {
            throw new IllegalArgumentException(
                String.format("%s %s must have minimum salary %.2f\n", 
                    this.role, this.name, MINIMUM_SALARY)
            );
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    protected boolean hasValidSalary() {
        return this.salary >= MINIMUM_SALARY;
    }
}
