package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    public ThickBunBurger() {
        this.description = "Thick Bun Burger";
    }

    // @Override
    // public String getDescription() {
    //     String description = this.description;
    //     if (this.hasBeefMeat) description += ", adding meat";
    //     if (this.hasCheese) description += ", adding cheese";
    //     return description;
    // }

    @Override
    public double cost() {
        return 2.5;

        // double cost = 2.5;
        // if (this.hasBeefMeat) cost += 6.0;
        // if (this.hasCheese) cost += 2.0;
        // return cost;
    }
}
