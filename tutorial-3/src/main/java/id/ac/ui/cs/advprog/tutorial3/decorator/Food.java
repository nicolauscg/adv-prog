package id.ac.ui.cs.advprog.tutorial3.decorator;

public abstract class Food {
    protected String description = "Unidentified Food";
    // public boolean hasCheese = false;
    // public boolean hasBeefMeat = false;
    // other fillings omitted for brevity

    public String getDescription() {
        return description;
    }

    public abstract double cost();
}
