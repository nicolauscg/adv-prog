package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
// import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class FrontendProgrammer extends Employees {
    private static final double MINIMUM_SALARY = 30000.00;

    public FrontendProgrammer(String name, double salary) throws IllegalArgumentException {
        this.name = name;
        this.salary = salary;
        this.role = "Front End Programmer";
        if (!hasValidSalary()) {
            throw new IllegalArgumentException(
                String.format("%s %s must have minimum salary %.2f\n", 
                    this.role, this.name, MINIMUM_SALARY)
            );
        }
    }

    @Override
    public double getSalary() {
        return this.salary;
    }

    @Override
    protected boolean hasValidSalary() {
        return this.salary >= MINIMUM_SALARY;
    }
}
