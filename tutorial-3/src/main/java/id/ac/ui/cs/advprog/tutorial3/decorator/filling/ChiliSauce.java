package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ChiliSauce extends Filling {
    public ChiliSauce(Food food) {
        this.food = food;
        this.description = "chili sauce";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.seperator + this.description;
    }

    @Override
    public double cost() {
        return 0.3 + this.food.cost();
    }
}
