package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class Lettuce extends Filling {
    public Lettuce(Food food) {
        this.food = food;
        this.description = "lettuce";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.seperator + this.description;
    }

    @Override
    public double cost() {
        return 0.75 + this.food.cost();
    }
}
