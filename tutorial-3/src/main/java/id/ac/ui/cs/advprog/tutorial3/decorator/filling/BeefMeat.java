package id.ac.ui.cs.advprog.tutorial3.decorator.filling;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class BeefMeat extends Filling {
    public BeefMeat(Food food) {
        this.food = food;
        this.description = "beef meat";
    }

    @Override
    public String getDescription() {
        return this.food.getDescription() + this.seperator + this.description;
    }

    @Override
    public double cost() {
        return 6.0 + this.food.cost();
    }
}
