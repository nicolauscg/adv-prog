package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

public class Main {
    public static void main(String[] args) {
        Company adpro = new Company();
        
        Ceo ceo1 = new Ceo("ceo1", 200001.0);
        adpro.addEmployee(ceo1);
        try {
            Ceo ceo2 = new Ceo("ceo2", 190001.0);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException ceo salary");
        }

        FrontendProgrammer frontend1 = new FrontendProgrammer("frontend1", 30001.0);
        adpro.addEmployee(frontend1);
        try {
            FrontendProgrammer frontend2 = new FrontendProgrammer("frontend2", 29001.0);
        } catch (IllegalArgumentException e) {
            System.out.println("IllegalArgumentException frontend salary");
        }

        System.out.println("net salary: " + adpro.getNetSalaries());
    }
}