package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class DoughTest {
    private Class<?> doughClass;

    @Before
    public void setUp() throws Exception {
        doughClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough");
    }

    @Test
    public void testDoughIsInterface() throws Exception {
        assertTrue(Dough.class.isInterface()); 
    }

    @Test
    public void testCheeseHasToStringMethod() throws Exception {
        Method toString = doughClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}