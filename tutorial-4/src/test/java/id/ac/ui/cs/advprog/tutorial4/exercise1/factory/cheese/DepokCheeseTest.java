package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DepokCheeseTest {
    private DepokCheese depokCheese;

    @Before
    public void setUp() {
        depokCheese = new DepokCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Depok Specialty Cheese", depokCheese.toString());
    }
}
