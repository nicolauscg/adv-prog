package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.DepokCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.DepokClam;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.DepokDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.DepokSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.DepokVeggie;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class DepokPizzaIngredientFactoryTest {
    private DepokPizzaIngredientFactory depokPizzaIngredientFactory;

    @Before
    public void setUp() throws Exception {
        depokPizzaIngredientFactory = new DepokPizzaIngredientFactory();
    }
    
    @Test
    public void testMethodCreateDough() {
        assertTrue(depokPizzaIngredientFactory.createDough() instanceof DepokDough);
    }

    @Test
    public void testMethodCreateSauce() {
        assertTrue(depokPizzaIngredientFactory.createSauce() instanceof DepokSauce);
    }

    @Test
    public void testMethodCreateCheese() {
        assertTrue(depokPizzaIngredientFactory.createCheese() instanceof DepokCheese);
    }

    @Test
    public void testMethodCreateVeggies() {
        Veggies[] veggieByFactory = depokPizzaIngredientFactory.createVeggies();
        Class[] veggieClass = new Class[]{DepokVeggie.class, Onion.class, Mushroom.class};
        for (int i = 0; i < veggieClass.length; i++) {
            assertEquals(veggieByFactory[i].getClass(), veggieClass[i]);
        }
    }

    @Test
    public void testMethodCreateClam() {
        assertTrue(depokPizzaIngredientFactory.createClam() instanceof DepokClam);
    }
}
