package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DepokVeggieTest {
    private DepokVeggie depokVeggie;

    @Before
    public void setUp() {
        depokVeggie = new DepokVeggie();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Depok Specialty Veggies", depokVeggie.toString());
    }
}
