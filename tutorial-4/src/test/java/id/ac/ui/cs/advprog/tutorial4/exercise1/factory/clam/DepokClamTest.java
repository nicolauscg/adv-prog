package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DepokClamTest {
    private DepokClam depokClam;

    @Before
    public void setUp() {
        depokClam = new DepokClam();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Depok Specialty Clams", depokClam.toString());
    }
}
