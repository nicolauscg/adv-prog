package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.ReggianoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.FreshClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MarinaraSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Garlic;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Mushroom;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Onion;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.RedPepper;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import org.junit.Before;
import org.junit.Test;

public class NewYorkPizzaIngredientFactoryTest {
    private NewYorkPizzaIngredientFactory newYorkPizzaIngredientFactory;

    @Before
    public void setUp() throws Exception {
        newYorkPizzaIngredientFactory = new NewYorkPizzaIngredientFactory();
    }
    
    @Test
    public void testMethodCreateDough() {
        assertTrue(newYorkPizzaIngredientFactory.createDough() instanceof ThinCrustDough);
    }

    @Test
    public void testMethodCreateSauce() {
        assertTrue(newYorkPizzaIngredientFactory.createSauce() instanceof MarinaraSauce);
    }

    @Test
    public void testMethodCreateCheese() {
        assertTrue(newYorkPizzaIngredientFactory.createCheese() instanceof ReggianoCheese);
    }

    @Test
    public void testMethodCreateVeggies() {
        Veggies[] veggieByFactory = newYorkPizzaIngredientFactory.createVeggies();
        Class[] veggieClass = new Class[]
            { Garlic.class, Onion.class, Mushroom.class, RedPepper.class };
        for (int i = 0; i < veggieClass.length; i++) {
            assertEquals(veggieByFactory[i].getClass(), veggieClass[i]);
        }
    }

    @Test
    public void testMethodCreateClam() {
        assertTrue(newYorkPizzaIngredientFactory.createClam() instanceof FreshClams);
    }
}
