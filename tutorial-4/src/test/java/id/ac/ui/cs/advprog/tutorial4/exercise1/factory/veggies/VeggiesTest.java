package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class VeggiesTest {
    private Class<?> veggiesClass;

    @Before
    public void setUp() throws Exception {
        veggiesClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
    }

    @Test
    public void testDoughIsInterface() throws Exception {
        assertTrue(Veggies.class.isInterface()); 
    }

    @Test
    public void testCheeseHasToStringMethod() throws Exception {
        Method toString = veggiesClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}