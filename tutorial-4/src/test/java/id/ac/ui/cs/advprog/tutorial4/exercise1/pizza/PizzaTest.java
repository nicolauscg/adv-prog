package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class PizzaTest {
    private Class<?> pizzaClass;

    @Before
    public void setUp() throws Exception {
        pizzaClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza");
    }

    @Test
    public void testPizzaIsAbstract() throws Exception {
        int classModifiers = pizzaClass.getModifiers();

        assertTrue(Modifier.isAbstract(classModifiers)); 
    }

    @Test
    public void testPizzaHasBakeMethod() throws Exception {
        Method bake = pizzaClass.getDeclaredMethod("bake");
        int methodModifiers = bake.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", bake.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaHasCutMethod() throws Exception {
        Method cut = pizzaClass.getDeclaredMethod("cut");
        int methodModifiers = cut.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", cut.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaHasBoxMethod() throws Exception {
        Method box = pizzaClass.getDeclaredMethod("box");
        int methodModifiers = box.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", box.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaHasSetNameMethod() throws Exception {
        Method setName = pizzaClass.getDeclaredMethod("setName", String.class);
        int methodModifiers = setName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("void", setName.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaHasGetNameMethod() throws Exception {
        Method getName = pizzaClass.getDeclaredMethod("getName");
        int methodModifiers = getName.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", getName.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaHasSetToStringMethod() throws Exception {
        Method toString = pizzaClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}