package id.ac.ui.cs.advprog.tutorial4.exercise1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.DepokCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.DepokClam;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.DepokVeggie;
import id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class PizzaStoreTest {
    private Class<?> pizzaStoreClass;

    @Before
    public void setUp() throws Exception {
        pizzaStoreClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.PizzaStore");
    }

    @Test
    public void testPizzaIsAbstract() throws Exception {
        int classModifiers = pizzaStoreClass.getModifiers();

        assertTrue(Modifier.isAbstract(classModifiers)); 
    }

    @Test
    public void testPizzaHasCreatePizzaMethod() throws Exception {
        Method createPizza = pizzaStoreClass.getDeclaredMethod("createPizza", String.class);
        int methodModifiers = createPizza.getModifiers();

        assertTrue(Modifier.isProtected(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", 
            createPizza.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaHasOrderPizzaMethod() throws Exception {
        Method orderPizza = pizzaStoreClass.getDeclaredMethod("orderPizza", String.class);
        int methodModifiers = orderPizza.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.pizza.Pizza", 
            orderPizza.getGenericReturnType().getTypeName());
    }

    @Test
    public void testOrderingCheesePizzaWithFactory() throws Exception {
        DepokPizzaStore depokPizzaStore = new DepokPizzaStore();
        Pizza pizza = depokPizzaStore.orderPizza("cheese");
        pizza.prepare();
        assertTrue(pizza.toString().contains(new DepokCheese().toString()));
    }

    @Test
    public void testOrderingVeggiePizzaWithFactory() throws Exception {
        DepokPizzaStore depokPizzaStore = new DepokPizzaStore();
        Pizza pizza = depokPizzaStore.orderPizza("clam");
        pizza.prepare();
        assertTrue(pizza.toString().contains(new DepokClam().toString()));
    }

    @Test
    public void testOrderingClamPizzaWithFactory() throws Exception {
        DepokPizzaStore depokPizzaStore = new DepokPizzaStore();
        Pizza pizza = depokPizzaStore.orderPizza("veggie");
        pizza.prepare();
        assertTrue(pizza.toString().contains(new DepokVeggie().toString()));
    }
}