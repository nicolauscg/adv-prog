package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class ClamsTest {
    private Class<?> clamsClass;

    @Before
    public void setUp() throws Exception {
        clamsClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
    }

    @Test
    public void testCheeseIsInterface() throws Exception {
        assertTrue(Clams.class.isInterface()); 
    }

    @Test
    public void testCheeseHasToStringMethod() throws Exception {
        Method toString = clamsClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}