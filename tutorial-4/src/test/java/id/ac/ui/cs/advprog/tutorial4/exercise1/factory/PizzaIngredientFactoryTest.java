package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class PizzaIngredientFactoryTest {
    private Class<?> pizzaIngredientFactoryClass;

    @Before
    public void setUp() throws Exception {
        pizzaIngredientFactoryClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory");
    }
    
    @Test
    public void testPizzaIngredientFactoryIsInterface() throws Exception {
        assertTrue(PizzaIngredientFactory.class.isInterface()); 
    }

    @Test
    public void testPizzaIngredientFactoryHasMethodCreateDough() throws Exception {
        Method toString = pizzaIngredientFactoryClass.getDeclaredMethod("createDough");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough", 
            toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaIngredientFactoryHasMethodCreateSauce() throws Exception {
        Method toString = pizzaIngredientFactoryClass.getDeclaredMethod("createSauce");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce", 
            toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaIngredientFactoryHasMethodCreateCheese() throws Exception {
        Method toString = pizzaIngredientFactoryClass.getDeclaredMethod("createCheese");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese", 
            toString.getGenericReturnType().getTypeName());
    }

    @Test
    public void testPizzaIngredientFactoryHasMethodCreateVeggies() throws Exception {
        Method toString = pizzaIngredientFactoryClass.getDeclaredMethod("createVeggies");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies[]", 
            toString.getGenericReturnType().getTypeName());
    }
    
    @Test
    public void testPizzaIngredientFactoryHasMethodCreateClam() throws Exception {
        Method toString = pizzaIngredientFactoryClass.getDeclaredMethod("createClam");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams", 
            toString.getGenericReturnType().getTypeName());
    }
}
