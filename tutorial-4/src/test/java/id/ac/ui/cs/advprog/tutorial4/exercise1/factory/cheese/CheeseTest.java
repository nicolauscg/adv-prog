package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.junit.Before;
import org.junit.Test;

public class CheeseTest {
    private Class<?> cheeseClass;

    @Before
    public void setUp() throws Exception {
        cheeseClass = Class.forName(
            "id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
    }

    @Test
    public void testCheeseIsInterface() throws Exception {
        assertTrue(Cheese.class.isInterface()); 
    }

    @Test
    public void testCheeseHasToStringMethod() throws Exception {
        Method toString = cheeseClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertEquals("java.lang.String", toString.getGenericReturnType().getTypeName());
    }
}