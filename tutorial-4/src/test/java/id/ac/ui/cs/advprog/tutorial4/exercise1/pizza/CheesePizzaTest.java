package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.DepokPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class CheesePizzaTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    
    @Test
    public void testMethodPrepare() {
        PizzaIngredientFactory ingredientFactory = new DepokPizzaIngredientFactory();
        Pizza cheesePizza = new CheesePizza(ingredientFactory);
        cheesePizza.prepare();
        System.out.println(cheesePizza);
        assertTrue(outContent.toString().contains(ingredientFactory.createCheese().toString()));
    }

    @Test
    public void testPizzaName() {
        CheesePizza cheesePizza = new CheesePizza(new DepokPizzaIngredientFactory());
        String pizzaName = "myPizza";
        cheesePizza.setName(pizzaName);
        assertEquals(cheesePizza.getName(), pizzaName);
    }
}
