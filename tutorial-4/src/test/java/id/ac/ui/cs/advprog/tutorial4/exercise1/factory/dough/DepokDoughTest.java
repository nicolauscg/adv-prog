package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DepokDoughTest {
    private DepokDough depokDough;

    @Before
    public void setUp() {
        depokDough = new DepokDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Depok Specialty Dough", depokDough.toString());
    }
}
