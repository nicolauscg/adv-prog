package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class DepokSauceTest {
    private DepokSauce depokSauce;

    @Before
    public void setUp() {
        depokSauce = new DepokSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Depok Specialty Sauce", depokSauce.toString());
    }
}
