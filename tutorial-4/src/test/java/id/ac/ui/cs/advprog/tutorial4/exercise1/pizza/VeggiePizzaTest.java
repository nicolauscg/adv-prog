package id.ac.ui.cs.advprog.tutorial4.exercise1.pizza;

import static org.junit.jupiter.api.Assertions.assertTrue;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.NewYorkPizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VeggiePizzaTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;
  

    @Before
    public void setUp() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }
    
    @Test
    public void testMethodPrepare() {
        PizzaIngredientFactory ingredientFactory = new NewYorkPizzaIngredientFactory();
        Pizza veggiePizza = new VeggiePizza(ingredientFactory);
        veggiePizza.prepare();
        System.out.println(veggiePizza);
        Veggies[] veggies = ingredientFactory.createVeggies();
        for (int i = 0; i < veggies.length; i++) {
            assertTrue(outContent.toString().contains(veggies[i].toString()));
        }
    }
}
