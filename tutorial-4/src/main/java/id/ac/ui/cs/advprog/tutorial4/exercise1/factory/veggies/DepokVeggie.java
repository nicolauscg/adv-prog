package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class DepokVeggie implements Veggies {

    public String toString() {
        return "Depok Specialty Veggies";
    }
}
